/**
 * Fichier Laby.java
 * Date: 6 Mars 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe concrète permettant le jeu de prendre forme.
 */
import java.util.Random;
import java.util.Scanner;
public class Laby
{
    /**
     * Fait patienter le programme
     * @param millisecondes: nombre de millisecondes à attendre.
     */
    public static void sleep(long millisecondes)
    {
        try {
            Thread.sleep(millisecondes);
        }
        catch(InterruptedException e){
            System.out.println("Sleep interrompu");
        }
    }

    /**
     *  *
     * Construit les murs du vrai labyrinthe
     * S'assure que le labyrinthe jeu affiche et ait la même ouverture.
     * Met un personnage dedans le vrai labyrinthe.
     * L'affiche
     * Le fait disparaitre
     * et efface le personnage
     *
     * @param p: Référence du personnage du jeu.
     * @param labyV: Référence du labyrinthe Vrai
     * @param labyJ: Référence du labyrinthe jeu
     * @param tps: Valeur des secondes à faire patienter le programme
     * @param dens: Valeur de la densité qu'aura le labyrinthe Vrai.
     */

    public static void commencement(Personnage p, Labyrinthe labyV, Labyrinthe labyJ,  int tps, double dens)
    {

        labyV.construitLabyrintheAleatoire(dens);
        labyJ.setOuverture(labyV.getOuverture());
        labyJ.dessineOuverture(labyV.getOuverture());

        labyV.dessinePersonnage(p);
        labyV.affiche();
        sleep(tps*1000);
        Labyrinthe.effaceEcran();
        labyV.effacePersonnage(p);

    }

    /**
     * Teste si un muret existe selon la case demander et sa direction;
     * si c'est cas, lance actionMur et retourne true;
     * Sinon retourne seulement false.
     *
     * @param vert_hori: Indique si on regarde pour un muret vertical (1) ou un muret horizontal (2)
     * @param x: Indice de la colonne de la case à vérifier
     * @param y: Indice de la rangée de la case à vérifier
     * @param p: Référence du personnage.
     * @param laby: Référence au labyrinthe Vrai (Contient les murs)
     * @return: True, s'il y a un mur et False, s'il n'y en a pas.
     */
    public static boolean test_mur(int vert_hori, int x, int y, Personnage p, Labyrinthe laby)
    {
        boolean succes = false;
        if(vert_hori == 1) // Vertical
        {
            succes = laby.muretVerticalExiste(x,y);

        }
        else if(vert_hori == 2) //Horizontal
        {
            succes = laby.muretHorizontalExiste(x,y);
        }
        if(succes)
        {
            actionMur(vert_hori, x, y, p);
        }
        return succes;
    }

    /**
     * Fait perdre une vie au personnage
     * et dessine le muret que personnage à frapper.
     *
     * @param p: Référence au personnage.
     * @param vert_hori: Indique si on dessine pour un muret vertical (1) ou un muret horizontal (2)
     */

    public static void actionMur(int vert_hori, int x, int y, Personnage p)
    {
        p.perteNbVies();

        if(vert_hori == 1) // Vertical
        {
            p.getLabyrinthe().dessineMuretVertical(x,y);

        }
        else if(vert_hori == 2) //Horizontal
        {
            p.getLabyrinthe().dessineMuretHorizontal(x,y);
        }

    }


    /**
     *
     * Méthode "critere d'arret"
     * s'assure que le nombre de vie soit plus grand que 0 et mentionne si le joueur à perdu
     * s'assure que si le prochain mouvement amène le personnage à être à la case de l'ouverture.
     *
     * @param larg: Valeur de la largeur du labyrinthe
     * @param p: Référence du personnage.
     * @return: True ou False pour indiquer la fin ou la suite de la partie.
     */
    public static boolean gagner_perdu( int larg, Personnage p, int nbVies)
    {
        boolean suite = false;
        if (p.getNbVies() == 0) {
            p.getLabyrinthe().dessinePersonnageMort(p);
            p.getLabyrinthe().affiche();
            System.out.println("(Vous êtes mort. Vous avez épuisé vos "+nbVies+" vies!)");
            suite = true;
        }
        if( p.getColonne() == larg-1 && p.getRangee() == p.getLabyrinthe().getOuverture() )
        {
            int nbErreurs = nbVies - p.getNbVies();
            System.out.println("Bravo, vous êtes parvenu jusqu'à la sortie en ne commettant que " +nbErreurs+ " erreurs.");
            suite = true;

        }
        return suite;
    }

    /**
     *S'assure du visuel de la partie et de l'interaction joueur/programme.
     *
     *
     * @param laby: Référence au Labyrinthe auquel il y a le jeu.
     * @param p: Référence au personnage.
     * @param nbVies: Valeur de départ du nombre de vie (seulement pour l'affichage)
     * @return: Un String de la réponse du joueur.
     */
    public static String visuel(Labyrinthe laby, Personnage p, int nbVies)
    {
        Scanner scan = new Scanner(System.in);
        laby.affiche();
        System.out.println(" Il vous reste "+p.getNbVies()+ " vies sur " + nbVies + ".");
        System.out.println();
        System.out.println("Quelle direction souhaitez-vous prendre?");
        System.out.println("(droite: d; gauche: g ou s; haut: h ou e; bas: b ou x)");
        String rep = scan.nextLine();
        return rep.toLowerCase();


    }

    /**
     * Assure le visuel lorsque l'intelligence artificiel est enclenché.
     *
     * @param p: Référence du Personnage.
     * @param aq: Référence à l'arbre quaternaire
     */
    public static void visuel2( Personnage p, ArbreQuat aq){

        p.getLabyrinthe().affiche();
        System.out.println("Intelligence artificielle au travail; S.v.p. ne pas déranger.");
        System.out.println("Le chemin qu'elle suivra:");
        aq.printBestPath();
        sleep(500);

    }

    /**
     * Lancement de l'intellgence artificiel.
     * (Fait les instructions du tableau aq.getBestPath() si un chemin existe)
     *
     * @param p: Référence du personnage.
     * @param aq: Réféence à l'arbre
     */
    public static void intelligence(Personnage p, ArbreQuat aq){
        if(aq.getCheminExiste())
        {
            System.out.println("Un chemin est possible.");
            eq: for(int i = 1; i < (aq.getBestPath()).length ; i++)
            {
                switch(aq.getBestPath()[i])
                {
                    case "d":
                        p.deplacementDroite();
                        visuel2( p, aq);
                        continue eq;
                    case "h":
                        p.deplacementHaut();
                        visuel2( p, aq);
                        continue eq;
                    case "g":
                        p.deplacementGauche();
                        visuel2( p, aq);
                        continue eq;
                    case "b":
                        p.deplacementBas();
                        visuel2( p, aq);
                        continue eq;
                }
            }

            (p.getLabyrinthe()).affiche();
            System.out.println("Voila!");
        }
        else aq.printBestPath();


    }

    /**
     * Lance la partie et la fin de la fonction demande au joueur s'il veut jouer une nouvelle partie
     * avec les même paramètre ou pas
     *
     *
     * @param args: Paramètres du jeu.
     */

    public static void main (String[] args) {
        Scanner scan = new Scanner(System.in);
        boolean fin = true;

        while (fin) {
            int h = Integer.parseInt(args[0]);
            int l = Integer.parseInt(args[1]);
            double densite = Double.parseDouble(args[2]);
            int duree = Integer.parseInt(args[3]);
            int nbVies = Integer.parseInt(args[4]);

            fin = jeuLabyrintheInvisible(h, l, densite, duree, nbVies);

            if (fin) {
                System.out.println("Voulez-vous jouer une nouvelle partie?(Oui/Non)");
                String bonneRep = (scan.nextLine()).toLowerCase();
                switch (bonneRep) {
                    case "o":
                    case "oui":
                        System.out.println("Avec les mêmes paramètres?(Oui/Non)");
                        String rep = (scan.nextLine()).toLowerCase();
                        switch (rep) {
                            case "o":
                            case "oui":
                                break;
                            case "n":
                            case "non":
                                System.out.println("Veuillez rentrer 5 nouveaux paramètres.");
                                System.out.println("(Hauteur, Largeur, Densite, Duree, Nombre de vies)");
                                String s = (scan.nextLine()).toLowerCase();
                                args = s.split(" ");
                                break;
                            default:
                                System.out.println("Réponse invalide, une autre partie commencera avec les mêmes paramètres.");
                                sleep(2000);
                                break;
                        }
                        break;
                    case "n":
                    case "non":
                        fin = false;
                        break;
                    default:
                        System.out.println("Réponse invalide, alors une autre partie commencera avec les mêmes paramètres.");
                        sleep(2000);
                        break;
                }

            }


        }
    }

    /**
     *
     * Fonction qui gère le déroulement du jeu avec l'aide des fonctions ci-dessus
     *
     * @param haut: Valeur de la hauteur du labyrinthe.
     * @param larg: Valeur de la largeur du labyrinthe.
     * @param dens: Valeur de la densité des murets du labyrinthe
     * @param tps: Valeur des secondes d'affichage
     * @param nbVies: Valeur du nombre de vies du personnage (Chances)
     * @return: True ou False tout dépend si le joueur veut jouer une autre partie ou pas.
     */
    public static boolean jeuLabyrintheInvisible(int haut, int larg, double dens, int tps, int nbVies)
    {
        Random rand = new Random();
        boolean suite = true;

        Labyrinthe labyJeu = new Labyrinthe(haut,larg);  // Initialise les labyrinthes du jeu et le personnage.
        Labyrinthe labyVrai = new Labyrinthe(haut,larg);
        Personnage perso = new Personnage(labyJeu,rand.nextInt(haut), 0, nbVies);


        commencement(perso, labyVrai, labyJeu, tps, dens); // Fait la création du labyrinthe et l'affichage de celui-ci.



        while(suite)
        {
            String bonneRep = visuel(labyJeu, perso, nbVies);
            switch (bonneRep) { // Recoit la commande du joueur
                case "d": // Droite
                    if(!test_mur(1, perso.getColonne()+1, perso.getRangee(), perso, labyVrai)){ // Regarde si mur
                        if(!(gagner_perdu(larg, perso, nbVies))) { // regarde s'il n'a pas gagné
                            perso.deplacementDroite(); // Déplacement
                        }
                        else
                        {
                            suite = false;  // Si gagner arrêt de la boucle while donc arrêt de jeuLabyrintheInvisible
                        }

                    }
                    else if(gagner_perdu( larg, perso, nbVies)) // Regarde s'il a perdu
                    {
                        suite = false; // Si oui, arrêt de la boucle...
                    }
                    break;
                case "g": // gauche
                case "s":
                    if(!test_mur(1, perso.getColonne(), perso.getRangee(), perso, labyVrai)){
                        if(!gagner_perdu( larg, perso, nbVies)) {
                            perso.deplacementGauche();
                        }
                        else
                        {
                            suite = false;
                        }
                    }
                    else if(gagner_perdu( larg, perso, nbVies))
                    {
                        suite = false;
                    }
                    break;
                case "e": // Haut
                case "h":
                    if(!test_mur(2, perso.getColonne(), perso.getRangee(), perso, labyVrai)){
                        if(!gagner_perdu( larg, perso, nbVies)) {  //
                            perso.deplacementHaut();
                        }
                        else
                        {
                            suite = false;
                        }
                    }
                    else if(gagner_perdu( larg, perso, nbVies))
                    {
                        suite = false;
                    }
                    break;
                case "b": //bas
                case "x":
                    if(!test_mur(2, perso.getColonne(), perso.getRangee()+1, perso, labyVrai)){
                        if(!gagner_perdu( larg, perso, nbVies)) {  //
                            perso.deplacementBas();
                        }
                        else
                        {
                            suite = false;
                        }
                    }
                    else if(gagner_perdu( larg, perso, nbVies))
                    {
                        suite = false;
                    }
                    break;

                case "q": // Alors jeuLabyrinthe retourne false  (Arrêt de while et On ne rentrera pas dans le if dans main)
                    return false;

                case "p": // Fait seulement l'arrêt de la boucle while
                    suite = false;
                    break;


                case "v": // Recopie LabyVrai dans Labyjeu et on change le laby dans le personnage
                    labyJeu = new Labyrinthe(labyVrai);
                    perso.nouveauLaby(labyJeu);
                    break;

                case "o": // Copie labyVrai dans l'ArbreQuat, lance intellgence et fait l'arrêt de la boucle.
                    labyJeu = new Labyrinthe(labyVrai);
                    perso.nouveauLaby(labyJeu);


                    ArbreQuat aQ = new ArbreQuat(perso);
                    intelligence(perso, aQ);
                    suite = false;
                    break;

                default:
                    System.out.println("Commande invalide");
                    sleep(500);
                    break;
            }
        }
        return true; // Donc, on rentre dans le if de main (pour savoir si on rejoue)
    }
}