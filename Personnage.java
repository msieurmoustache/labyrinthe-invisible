/**
 * Fichier Personnage.java
 * Date: 6 Mars 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe concrète créant des objets Personnage.
 */
public class Personnage
{
    private int i, j;
    private Labyrinthe laby;
    private int nbVies;

    /**
     * Constructeur de Personnage:
     *
     * @param l: L'objet Labyrinthe où le personnage sera.
     * @param ii: Indice de la colonne où sera le personnage.
     * @param jj: Indice de la rangée où sera le personnage.
     * @param vies: Nombre de vies qu'à l'objet
     */
    public Personnage(Labyrinthe l, int ii, int jj, int vies)
    {
        laby = l;
        i = ii;
        j = jj;
        nbVies = vies;
        laby.dessinePersonnage(this);
    }

    /**
     * Accès aux attributs de Personnage.
     *
     */
    public int getColonne()
    {
        return j;
    }
    public int getRangee()
    {
        return i;
    }
    public int getNbVies()
    {
        return nbVies;
    }
    public Labyrinthe getLabyrinthe()
    {
        return laby;
    }

    /**
     * Donne un nouveau labyrinthe à personnage et ajoute le personnage à ce labyrinthe.
     *
     * @param lab: objet Labyrinthe étant le nouveau Labyrinthe.
     */
    public void nouveauLaby(Labyrinthe lab)
    {
        laby = lab;
        laby.dessinePersonnage(this);
    }

    /**
     * Diminue de un le nombre de vies du personnage.
     */
    public void perteNbVies()
    {
        nbVies--;
    }


    /**
     * Effectue le déplacement à droite du Personnage dans le labyrinthe
     */
    public void deplacementDroite()
    {
        laby.effacePersonnage(this);
        j++;
        laby.dessinePersonnage(this);

    }

    /**
     * Effectue le déplacement en haut du Personnage dans le labyrinthe
     */
    public void deplacementHaut()
    {
        laby.effacePersonnage(this);
        i--;
        laby.dessinePersonnage(this);
    }

    /**
     * Effectue le déplacement à gauche du Personnage dans le labyrinthe
     */
    public void deplacementGauche()
    {
        laby.effacePersonnage(this);
        j--;
        laby.dessinePersonnage(this);
    }

    /**
     * Effectue le déplacement à droite du Personnage dans le labyrinthe
     */
    public void deplacementBas()
    {
        laby.effacePersonnage(this);
        i++;
        laby.dessinePersonnage(this);
    }


}