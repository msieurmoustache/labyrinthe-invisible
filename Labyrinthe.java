/**
 * Fichier Labyrinthe.java
 * Date: 6 Mars 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe concrète créant des objets de type Labyrinthe.
 */


public class Labyrinthe
{
	private char[][] lab; int hauteur; int largeur; int ouverture;
	private static int LMURET = 8;
	private static int HMURET = 4;

	/**Constructeur du labyrinthe
	 *
	 * @param h: valeur de la hauteur
	 * @param w: valeur de la largeur
     */
	public Labyrinthe (int h, int w)
	{
		hauteur = h;
		largeur = w;
		creeTableau(hauteur, largeur);
		effaceTableau();
		dessineMurdEnceinte();
	}
	/** Constructeur de copies de labyrinthe
	 * Copie toutes les valeurs de l dans this.
     * @param l: Référence du labyrinthe
     *
     */
	public Labyrinthe(Labyrinthe l)
	{
		hauteur = l.hauteur;
		largeur = l.largeur;
		creeTableau(hauteur,largeur);
		for (int i =0;i<l.lab.length;i++)
		{
			for (int j = 0;j<l.lab[0].length;j++)
			{
				lab[i][j] = l.lab[i][j];
			}
		}
		ouverture = l.getOuverture();
	}

	/**Acces aux proprietes
	 *
	 * @return: valeur demandé
     */
	public int getLargeur()
	{
		return largeur;
	}

	public int getHauteur()
	{
		return hauteur;
	}

	public int getOuverture()
	{
		return ouverture;
	}

	/**
	 *  Change l'ouverture du labyrinthe
	 *
	 * @param o: Indice de la rangée de la nouvelle ouverture
     */
	public void setOuverture(int o){
		ouverture = o;
	}

	/**
	 * Attribue les dimensions du tableau et le remplit d'espaces
	 *
	 * @param hauteur: Valeur de la hauteur
	 * @param largeur: Valeur de la largeur
     */

	public void creeTableau (int hauteur, int largeur)
	{
		lab = new char[largeur*LMURET+1][hauteur*HMURET+1];

		effaceTableau();
	}

	/**
	 * Vide le labyrinthe en remplaçant toutes ses cases par des espaces
	 *
	 */
	public void effaceTableau ()
	{
		String s = " ";
		for (int i =0; i<lab.length; i++)
		{
			for (int j =0; j<(lab[0]).length;j++)
			{
				lab[i][j]=s.charAt(0);
			}
		}
	}

	/**
     * Les fonctions suivantes ne prennent aucun paramètre et ajoute les caractères nécessaires au tableau char
     * pour dessiner les murs et les personnages.
     */
	public void dessineMurdEnceinte()
	{
		String plus = "+";
		String moins = "-";
		String barre = "|";
		for (int i = 0; i<=hauteur*HMURET;i++)
		{
			for (int j = 0; j<=largeur*LMURET;j++)
			{

				if( i == 0 || i == hauteur*HMURET)
				{
					lab[j][i] = moins.charAt(0);
				}

				if(j==0||j==largeur*LMURET)
				{
					lab[j][i] = barre.charAt(0);
				}
				if ((i==0||i==hauteur*HMURET) && (j==0||j==largeur*LMURET))
				{
					lab[j][i] = plus.charAt(0);
				}

			}
		}
	}
	public void dessineOuverture(int j)
	{
		String s = " ";



		for (int i = 1;i<HMURET;i++)
		{
			lab[largeur*LMURET][(j*HMURET)+i]=s.charAt(0);
		}
	}
	public void dessineMuretVertical (int i, int j)
	{
		String barre = "|";
		for (int k = 1;k<HMURET;k++)
		{
			lab[i*LMURET][(j*HMURET)+k]=barre.charAt(0);
		}
	}
	public void dessineMuretHorizontal (int i, int j)
	{
		String moins = "-";
		for (int k = 1;k<LMURET;k++)
		{
			lab[(i*LMURET)+k][j*HMURET]=moins.charAt(0);
		}
	}

	public void dessinePersonnage(Personnage p)
	{
		String a = "@";
		lab[p.getColonne()*LMURET+LMURET/2][p.getRangee()*HMURET+HMURET/2] = a.charAt(0);
	}
	public void effacePersonnage(Personnage p)
	{
		String s = " ";
		lab[p.getColonne()*LMURET+LMURET/2][p.getRangee()*HMURET+HMURET/2] = s.charAt(0);
	}

	public void dessinePersonnageMort(Personnage p)
	{
		String a = "X";
		lab[p.getColonne()*LMURET+LMURET/2][p.getRangee()*HMURET+HMURET/2] = a.charAt(0);
	}


	/**
     * Les deux fonctions suivantes testent la présence de murs en vérifiant le caractère présent à l'endroit ou il pourrait y avoir un mur.
	 * Les murs verticaux sont testés à gauche et les murs horizontaux sont testés de la cellule correspondant à i,j.
	 *
	 * @param i: Indice de la colonne testée
     * @param j: Indice de la rangée testée.
     *
     */
	public boolean muretVerticalExiste (int i, int j)
	{
		String barre = "|";
		if (lab[i*LMURET][j*HMURET+1]== barre.charAt(0))
		{
			return true;
		}
		else return false;

	}
	public boolean muretHorizontalExiste (int i, int j)
	{
		String moins = "-";
		if (lab[i*LMURET+1][j*HMURET]==moins.charAt(0)) return true;
		else return false;
	}

/*
 * Imprime une quantité arbitrairement grande de lignes pour effacer l'affichage précédent.
 */

	public static void effaceEcran()
	{
		for (int i = 0;i<200;i++)
		{
			System.out.println("");
		}
	}

	/*
     *  Imprime un string qui est composé de toutes les cases du tableau de char, dont les rangées sont séparées par un retour à la ligne.
     *  Au final, cela affiche le labyrinthe.
     */
	public void affiche()
	{
		String s="";
		for (int i =0; i<=hauteur*HMURET;i++)
		{
			for (int j =0; j<=largeur*LMURET;j++)
			{
				s+=lab[j][i];
			}
			s+="\n";
		}
		effaceEcran();
		System.out.println(s);

	}

	/**
	 *  Passe à travers toutes les cases du labyrinthe et défini en comparant à la densité si un mur existera ou non
	 *  t crée une ouverture au hasard.
	 *
	 * @param densite: Valeur double qui correspond au pourcentage de chance qu'un mur existe pour chaque possibilité de mur.
     */

	public void construitLabyrintheAleatoire(double densite)
	{
		for (int i =0;i<largeur;i++)
		{
			for (int j=0;j<hauteur;j++)
			{
				if (Math.random() < densite)
				{
					dessineMuretHorizontal(i,j);
				}
				if (Math.random() < densite)
				{
					dessineMuretVertical(i,j);
				}
			}
		}
		ouverture = (int) (Math.round(Math.random()*(hauteur-1)));
		dessineOuverture(ouverture);
	}

}