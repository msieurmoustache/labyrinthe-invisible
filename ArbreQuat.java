/**
 * Fichier ArbreQuat.java
 * Date: 6 Mars 2016
 *
 * @author Félix Bélanger-Robillard, Lauranne Deaudelin
 * @version 1.0
 * @since 1.8.0_65
 *
 * Classe concrète créant des objets de type ArbreQuat à partir de Labyrinthe.
 * (Intelligence artificielle)
 */
public class ArbreQuat {
	private ArbreQuat up; ArbreQuat down; ArbreQuat right; ArbreQuat left; 
	private int i; int j;
	private Labyrinthe laby;
	private boolean[][] visitedCells;
	private boolean cheminExiste;
	private String[] bestPath;
	private int compteur;
	private String directionP;
	private boolean undefined;
	
/**
 * Premier constructeur
 *
 * À partir des attributs du personnage, le constructeur transforme le labyrinthe associé en une arbre 
 * pour pouvoir ensuite trouver le chemin le plus rapide pour sortir du labyrinthe.
 *
 * @param p: Référence au personnage p
 * 
 */
	public ArbreQuat (Personnage p) 
	{
		compteur=0;
		undefined = false;
		j = p.getRangee();
		i = p.getColonne();
		laby = p.getLabyrinthe();
		visitedCells = new boolean[laby.getLargeur()][laby.getHauteur()];
		for (int ii = 0; ii<laby.getLargeur();ii++) 
		{
			for (int jj = 0; jj<laby.getHauteur();jj++) 
			{
					visitedCells[ii][jj] = false;			
			}
		}
		visitedCells[i][j] = true;
		setFils();
		cheminExiste = cheminPos();
		if(cheminExiste) 
		{
			String[] s = new String[laby.getLargeur()*laby.getHauteur()+1];
			bestPath = shortestPath(s);
		}
		else 
		{
			bestPath = new String[2];
			bestPath[1] = "Aucun chemin disponible";
		}
	}
	
/**
 * Second constructeur
 *
 * @param ii: Valeur int de l'indice de la colonne de la case
 * @param jj: Valeur int de la rangée de la case
 * @param p: Référence de l'ArbreQuat p soit l'Arbre parent
 * @param direction: String direction correspond à la direction empruntée par le parent pour se retrouver là
 */
	public ArbreQuat ( int ii, int jj, ArbreQuat p, String direction) 
	{
		directionP = direction;
		compteur=p.getCompteur()+1;
		undefined=false;
		i = ii;
		j = jj;
		laby = p.getLabyrinthe();
		boolean[][] oldVisCells = p.getVisitedCells();
		visitedCells = new boolean[laby.getLargeur()][laby.getHauteur()];
		for (int a = 0; a<laby.getLargeur();a++) 
		{
			for (int b = 0; b<laby.getHauteur();b++) 
			{
					visitedCells[a][b] = oldVisCells[a][b];			
			}
		}
		visitedCells[i][j] = true;
		setFils();
		
	}
	
	/**
     *
	 * Troisieme constructeur.
     * Cree un enfant indefini pour terminer la recursivite de creation de fils.
     *
	 * @param ud: Boolean indiquant l'absence d'enfant
	 * @param p: Référence de l'ArbreQuat p soit l'Arbre parent
	 *
	 */
	public ArbreQuat (boolean ud, ArbreQuat p) 
	{
		undefined = true;
		laby = p.getLabyrinthe();
	}
	/**
	 * Vérifie la possibilité de déplacement dans les quatre directions
     * et définit les attributs up, down, right et left en conséquence.
	 */
	public void setFils () 
	{
		if (i<laby.getLargeur() && j<laby.getHauteur()) 
		{
			if (i+1<laby.getLargeur() ) 
				
			{
				if (!laby.muretVerticalExiste(i+1,j) & !isVisited(i+1,j)) right = new ArbreQuat(i+1,j, this,"d");
				else right = new ArbreQuat(true, this);
			}
			else right = new ArbreQuat(true, this);
			
			if (i-1>=0 )  
			{
				if (!laby.muretVerticalExiste(i,j) & !isVisited(i-1,j)) left = new ArbreQuat(i-1,j, this,"g");
				else left = new ArbreQuat(true, this);
			}
			else left = new ArbreQuat(true, this);
			
			if (j+1<laby.getHauteur()) 
			{
				if (!laby.muretHorizontalExiste(i,j+1) && !isVisited(i,j+1)) down = new ArbreQuat(i,j+1, this,"b");
				else down = new ArbreQuat(true, this);
				
			}
			else down = new ArbreQuat(true, this);
			
			if (j-1>=0 & (!laby.muretHorizontalExiste(i,j) && !isVisited(i,j-1)))	up = new ArbreQuat(i,j-1, this,"h");
			
			else up = new ArbreQuat(true, this);
		}
	}
	
	
	
	/**
	 * Shortest-path algorithm
     *
     * @param s: Référence d'un tableau vide.
     * @return : Tableau de String avec le meilleur chemin possible.
	 */
	public String[] shortestPath (String[] s) 
	{
		
		
		if(isVisited(laby.getLargeur()-1,laby.getOuverture()))
		{
			
				s = new String[compteur+1];	
			
			
			
		}
		
		else if(s.length == (laby.getLargeur()*laby.getHauteur()+1)) 
		{
			String[] u,d,l,r;
			if(!down.getUndefined()) 
			{
				d = down.shortestPath(s);
			}
			else 
			{
				d = s;
			}
			if(!up.getUndefined()) 
			{
				u = up.shortestPath(s);
			}
			else 
			{
				u = s;
			}
			if(!right.getUndefined()) 
			{
				r = right.shortestPath(s);
			}
			else 
			{
				r = s;
			}
			if(!left.getUndefined()) 
			{
				l = left.shortestPath(s);
			}
			else 
			{
				l = s;
			}
			s = d;
			if (u.length < s.length) s = u;
			if (r.length < s.length) s = r;
			if (l.length < s.length) s = l;
			
			
			
		}
		s[compteur] = directionP;
		return s;
		
		
	}
	
		
	/**
	 * Methode pour savoir si un chemin est possible.
     * @return : True ou False si un chemin est possible ou pas.
	 */
	
	public boolean cheminPos() 
	{
		if (undefined|laby.getLargeur()-1<0) return false;
		else 
		{
			if(!up.getUndefined()) 
			{
				if(up.cheminPos()) return true;
			}
			if(!down.getUndefined()&down.cheminPos()) return true;
			if(!right.getUndefined()&right.cheminPos()) return true;
			if(!left.getUndefined()&left.cheminPos()) return true;
			if (isVisited(laby.getLargeur()-1,laby.getOuverture())) return true;
			
		}
		
		return false; 
	}
	
	  /**
	    * Méthodes pour avoir accés aux différents attributs de ArbreQuat.
	    *
	    *  
	    */
	   public int getColonne()
	   {
	     return i;
	   }
	   public int getRangee()
	   {
	       return j;
	   }
	   public Labyrinthe getLabyrinthe()
	   {
		   return laby;
	   }
	   
	   public boolean[][] getVisitedCells()
	   {
		   return visitedCells;
	   }
	   
	   public int getCompteur()
		{
			return compteur;
		}
	   
	   public String getDirectionP()
		{
			return directionP;
		}
	   public boolean getUndefined() 
	   {
		   return undefined;
	   }
	   public boolean getCheminExiste()
	   {
		   return cheminExiste;
	   }
	   public String[] getBestPath()
	   {
		   String[] s= new String[bestPath.length];
		   for (int i = 1; i<bestPath.length;i++) 
		   {
			   s[i]=bestPath[i];
		   }
		   return s;
	   }
	   
	   public boolean isVisited(int i, int j) 
		{
			return visitedCells[i][j];
		}
	   
	   /**
        * Imprime les directions pour le chemin.
	    */
	   public void printBestPath() 
	   {
		   String s="";
		   for (int i = 1; i<bestPath.length;i++) 
		   {
			   s+=bestPath[i]+", ";
		   }
		   System.out.println(s);
	   }
	
}
